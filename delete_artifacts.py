#!/bin/python3
import json
import math
import os
from datetime import datetime, timedelta
from time import sleep
import urllib3

project_id = os.environ['CI_PROJECT_ID']
access_token = os.environ['PRIVATE_TOKEN']

headers = {"PRIVATE-TOKEN": access_token}
http = urllib3.PoolManager()


def is_success(status_code: int) -> bool:
    return math.floor(status_code / 200) == 1


def http_get_pages(url, page_num=1):
    has_next_page = True
    while has_next_page:
        params = {'page': page_num, 'per_page': 100}
        print(f'GET {url} page={page_num}')
        res = http.request("GET", url, fields=params, headers=headers)
        assert is_success(res.status), f"status code={res.status} reason={res.reason} data={res.data}"
        for page in json.loads(res.data.decode('utf-8')):
            yield page
        sleep(1)
        has_next_page = 'next' in res.headers['Link']
        page_num += 1


def http_delete(url):
    res = http.request("DELETE", url, headers=headers)
    assert is_success(res.status), f"status code={res.status} reason={res.reason}"


def should_delete(artifacts) -> bool:
    # Unfortunately, gitlab doesn't let us delete individual artifacts, so if one is bigger
    # than a megabyte, we'll delete all the artifacts in a job.
    for artifact in artifacts:
        if artifact["size"] > 1000000:  # if greater than 1Mb
            return True
    return False


def get_jobs_older_than(days=60, page=1):
    url = f"https://gitlab.com/api/v4/projects/{project_id}/jobs"
    days_ago = datetime.now() - timedelta(days=days)
    jobs = http_get_pages(url, page_num=page)
    for job in jobs:
        created_date = datetime.strptime(job['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
        if created_date < days_ago:
            yield job


def get_pipelines_older_than(days=60, page=1):
    url = f"https://gitlab.com/api/v4/projects/{project_id}/pipelines"
    days_ago = datetime.now() - timedelta(days=days)
    pipelines = http_get_pages(url, page_num=page)
    for pipeline in pipelines:
        if pipeline['status'] == 'running':
            continue
        created_date = datetime.strptime(pipeline['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
        if created_date < days_ago:
            yield pipeline


def delete_old_job_artifacts(days):
    for job in get_jobs_older_than(days=days):
        artifacts = job["artifacts"]
        if should_delete(artifacts):
            url = f"https://gitlab.com/api/v4/projects/{project_id}/jobs/{job['id']}/artifacts"
            print(f'DELETE {url} date={job["created_at"]} artifacts={artifacts}')
            http_delete(url)
            sleep(1)


def delete_old_pipelines(days):
    for pipeline in get_pipelines_older_than(days=days):
        url = f"https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline['id']}"
        print(f'DELETE {url} date={pipeline["created_at"]}')
        http_delete(url)
        sleep(1)


def main():
    delete_old_job_artifacts(60)
    delete_old_pipelines(60)


if __name__ == "__main__":
    main()
